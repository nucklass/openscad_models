use <MCAD/boxes.scad>
$fs = .4;
$fa = 1;

function inch_to_mm(in) = in * 25.4;
difference () {
    roundedBox([
        inch_to_mm(6.5), 
        175 ,
        10 + inch_to_mm(2)
        ],
        radius = 5,
        sidesonly = true,
        center=true
  );
    translate([0,0,-10]) {
        cube([
        inch_to_mm(6.5) - 20, 
        175 + 10,
        10 + inch_to_mm(2)
        ],
        center=true
    );
  }
    translate([0,0,25]){
       cylinder(10,40,40);
  }
}